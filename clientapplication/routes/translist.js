var passport =      require('passport'),
    login = require('./middlewares/login');

module.exports = function(app) {
  app.get('/translist',
    login.required,
    function(req, res) {
		
		////////////////////

		var mongoose = require('mongoose');
			mongoose.connect(process.env.DB_CON_URL);
			var db = mongoose.connection;
			db.on('error', console.error.bind(console, 'connection error:'));
			db.once('open', function (callback) {
			  console.log('DB open');
			});
			
			var transectionSchema = mongoose.Schema({
			Auth0Token: String,
								stripeToken: String,
								amount: String,
								currency: String,
								customerId: String,
								sourceName: String,
								sourceID: String,
								chargeId: String,
								balanceTransactionId: String
		});
		var transectionModel = mongoose.model('transections', transectionSchema);
		//End DB Work
		//var resdata ;
		transectionModel.find({}, function(err, alldata) {
		  if (err) 
		  {
			 console.log("exception occour fetching data "+ err);
			throw err;  
		  }

		  // object of all the users
		  console.log(alldata);
		  //resdata = alldata ;
		  res.render('translist', {
			user: req.user,
			results : alldata
		});
		  //response.render('pages/trans' ,{ results : alldata} );
		});
		
		
		/////////////////////
		
		
      
    });
}
